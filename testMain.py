#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 20:08:53 2018

@author: himanshu
"""

import pandas as pd
import time
import warnings
import os

from strategy.idtStrategy import IDTStrategy
from optimizer import Optimizer
from analyzer import Analyzer

if __name__ == "__main__":

    warnings.filterwarnings('ignore')
    
    tic = time.time()
    symbol = 'NIFTY'
    startYr = None
    endYr = None
    initCap = 100000
    strat = IDTStrategy(symbol, initCap, startYr=startYr, endYr=endYr)
    
#==============================================================================
#     combosGen = strat.combosGenerator()
#     combos= []
#     for combo in combosGen:
#         if combo[5] == combo[6]:
#             combos.append(combo)
#             
#     
#     opt = Optimizer(strat, combos)
#     opt.run()
#     
#     res = opt.getResults()
#     best = opt.getBestComboSharpe()
#     path = 'summary'+os.sep+symbol+os.sep+str(best[0])
#     if not os.path.exists(path):
#         os.makedirs(path)
#     s = pd.Series()
#     s.loc['Best Combo'] = best
#     
#     s.to_csv(path+os.sep+'BestCombo.csv')
#     res.to_csv(path+os.sep+'AllCombinations.csv')
#==============================================================================
    
    returns, buyAndHoldReturns, transactions = strat.run(best, True)
    
    a = Analyzer(initCap)
    a.createReturnsTearSheet(returns=returns, buyAndHoldReturns=buyAndHoldReturns)
    a.createRoundTripTearSheet(transactions)
    
    print(time.time() - tic)


#import pandas as pd
#f = pd.read_csv('data/15_mins/NIFTY.csv')