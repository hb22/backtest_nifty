#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 18:55:13 2018

@author: himanshu
"""

import pandas as pd
from multiprocessing import Pool

class Optimizer:
    
    def __init__(self, strategy, combos):
        
        self.__strategy = strategy
        self.__combos = combos
        self.__results = None
        self.__resultsDf = None

    def run(self):
        pool = Pool()
        self.__results = pool.map(self.__strategy.run, self.__combos)
    
    def getResults(self):
        for res in self.__results:
            if self.__resultsDf is None:
                self.__resultsDf = res
            else:
                self.__resultsDf = pd.concat([self.__resultsDf, res])
        return self.__resultsDf
    
    def getBestComboSharpe(self):
        maxParam = max(self.__resultsDf['Sharpe Ratio'])
        filterDf = self.__resultsDf.loc[self.__resultsDf['Sharpe Ratio']==maxParam]
        if len(filterDf) > 1:
            maxCagr = max(filterDf['CAGR'])
            filterDf = filterDf.loc[filterDf['CAGR']==maxCagr]
        
        return filterDf.index.tolist()[0]