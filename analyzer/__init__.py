#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 12:16:35 2018

@author: himanshu
"""

import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import scipy.stats as stats

from IPython.display import display

class Analyzer:

    def __init__(self, initialCapital, lots=1, commission=200.00):
        self.__initialCapital = initialCapital
        self.__lots = lots
        self.__commission = commission
        
    def createReturnsTearSheet(self, returns, buyAndHoldReturns=None):
            
        t1 = returns.index[0]
        t2 = returns.index[1]
        diff = t2-t1
        timeframe = (diff.seconds)/60
        
        ra = ReturnsAnalyzer(self.__initialCapital, timeframe)
        
        print("Entire data start date: " + str(returns.index[0].strftime('%Y-%m-%d')))
        print("Entire data end date: " + str(returns.index[-1].strftime('%Y-%m-%d')))
        
        annFactor = (252*6.25*60/timeframe)
        print('Backtest Months: ' + str( int((len(returns) / annFactor)*12) ) + '\n')
        
        ra.calcPerfStats(returns, buyAndHoldReturns)
        
        vertical_sections = 6
        
        fig = plt.figure(figsize=(20, vertical_sections * 6))
        gs = gridspec.GridSpec(vertical_sections, 3, wspace=0.5, hspace=0.5)
        axEquity = plt.subplot(gs[:2, :])
        
        i = 2
        axDrawdown = plt.subplot(gs[i, :], sharex=axEquity)
        i += 1
        axUnderwater = plt.subplot(gs[i, :], sharex=axEquity)
        i += 1
        axRollingSharpe = plt.subplot(gs[i, :], sharex=axEquity)
        i += 1
        axMonthlyHeatmap = plt.subplot(gs[i, 0])
        axAnnualReturns = plt.subplot(gs[i, 1])
        axMonthlyDist = plt.subplot(gs[i, 2])
        i += 1
        
        ra.plotEquity(returns, buyAndHoldReturns, ax=axEquity)
        ra.plotDrawdownPeriods(returns, ax=axDrawdown)
        ra.plotUnderwaterCurve(returns, ax=axUnderwater)
        ra.plotRollingSharpe(returns, ax=axRollingSharpe)
        ra.plotMonthlyReturnsHeatmap(returns, ax=axMonthlyHeatmap)
        ra.plotAnnualReturns(returns, ax=axAnnualReturns)
        ra.plotMonthlyReturnsDist(returns, ax=axMonthlyDist)
        
        ra.showWorstDrawdowns(returns)
        
        for ax in fig.axes:
            plt.setp(ax.get_xticklabels(), visible=True)

        plt.show()
        
        #return fig

    def createRoundTripTearSheet(self, transactions):
        rtAnalyzer = RoundTripAnalyzer(transactions, self.__lots, self.__commission)
        
        display(rtAnalyzer.getSummaryStats())
        display(rtAnalyzer.getPnLStats())
        display(rtAnalyzer.getDurationStats())
        display(rtAnalyzer.getReturnStats())
        
        fig = plt.figure(figsize=(14, 3 * 6))

        gs = gridspec.GridSpec(3, 2, wspace=0.5, hspace=0.5)
        
        axProbProfitTrade = plt.subplot(gs[0, :])
        axPnlPerRoundTripDollars = plt.subplot(gs[1, 0])
        axPnlPerRoundTripPct = plt.subplot(gs[1, 1])
        
        rtAnalyzer.plotProbProfitTrade(ax=axProbProfitTrade)
        
        transactions['Return'] = (transactions['PnL']*self.__lots)/transactions['Equity']
        
        sns.distplot(transactions.PnL, kde=False, ax=axPnlPerRoundTripDollars)
        axPnlPerRoundTripDollars.set(xlabel='PnL per round-trip trade in Rs.')
    
        sns.distplot(transactions.Return.dropna() * 100, kde=False, ax=axPnlPerRoundTripPct)
        axPnlPerRoundTripPct.set(xlabel='Round-trip returns in %')
        
        plt.show()        
        #return fig

class RoundTripAnalyzer:
    
    def __init__(self, transactions, lots, commission):
        self.__transactions = transactions.copy()
        self.__lots = lots
        self.__commission = commission
        
        self.__transactions['Return'] = (self.__transactions['PnL']*self.__lots)/self.__transactions['Equity']
        self.__shorts = self.__transactions.loc[self.__transactions['Trade']=='SELL']
        self.__longs = self.__transactions.loc[self.__transactions['Trade']=='BUY']
    
    def __getWinningTrades(self, transactions):
        return transactions.loc[transactions['Return'] > 0]
    
    def __getLosingTrades(self, transactions):
        return transactions.loc[transactions['Return'] < 0]
    
    def __getEvenTrades(self, transactions):
        return transactions.loc[transactions['Return'] == 0]
    
    def getSummaryStats(self):
        summaryStats = pd.DataFrame()
        summaryStats.loc['Total Number of Round Trips', 'All Trades'] = len(self.__transactions)
        summaryStats.loc['Total Number of Round Trips', 'Short Trades'] = len(self.__shorts)
        summaryStats.loc['Total Number of Round Trips', 'Long Trades'] = len(self.__longs)
        
        summaryStats.loc['Hit Ratio (%)', 'All Trades'] = len(self.__getWinningTrades(self.__transactions))*100/len(self.__transactions)
        summaryStats.loc['Hit Ratio (%)', 'Short Trades'] = len(self.__getWinningTrades(self.__shorts))*100/len(self.__shorts)
        summaryStats.loc['Hit Ratio (%)', 'Long Trades'] = len(self.__getWinningTrades(self.__longs))*100/len(self.__longs)
        
        summaryStats.loc['Winning Round Trips', 'All Trades'] = len(self.__getWinningTrades(self.__transactions))
        summaryStats.loc['Winning Round Trips', 'Short Trades'] = len(self.__getWinningTrades(self.__shorts))
        summaryStats.loc['Winning Round Trips', 'Long Trades'] = len(self.__getWinningTrades(self.__longs))
        
        summaryStats.loc['Losing Round Trips', 'All Trades'] = len(self.__getLosingTrades(self.__transactions))
        summaryStats.loc['Losing Round Trips', 'Short Trades'] = len(self.__getLosingTrades(self.__shorts))
        summaryStats.loc['Losing Round Trips', 'Long Trades'] = len(self.__getLosingTrades(self.__longs))
        
        summaryStats.loc['Even Round Trips', 'All Trades'] = len(self.__getEvenTrades(self.__transactions))
        summaryStats.loc['Even Round Trips', 'Short Trades'] = len(self.__getEvenTrades(self.__shorts))
        summaryStats.loc['Even Round Trips', 'Long Trades'] = len(self.__getEvenTrades(self.__longs))
    
        summaryStats.columns.name = 'Summary Stats'
        
        return np.round(summaryStats,2)
    
    def __getTotalProfit(self, transactions):
        return np.sum(transactions['PnL'])
    
    def __getGrossProfit(self, transactions):
        return np.sum(transactions['PnL'].loc[transactions['PnL'] > 0]+self.__commission)
    
    def __getGrossLoss(self, transactions):
        return np.sum(transactions['PnL'].loc[transactions['PnL'] < 0]+self.__commission)
    
    def __getAvgProfit(self, transactions, trades='All'):
        
        if trades == 'Winning':
            return np.mean(transactions['PnL'].loc[transactions['PnL'] > 0])
        
        if trades == 'Losing':
            return np.mean(transactions['PnL'].loc[transactions['PnL'] < 0])
        
        return np.mean(transactions['PnL'])
        
    def __getLargestTrade(self, transactions, winning=True):
        
        if not winning:
            return np.min(transactions['PnL'].loc[transactions['PnL'] < 0])
        
        return np.max(transactions['PnL'].loc[transactions['PnL'] > 0])
    
    def getPnLStats(self):
        pnlStats = pd.DataFrame()
        pnlStats.loc['Total Profit (in Rs.)', 'All Trades'] = self.__getTotalProfit(self.__transactions)
        pnlStats.loc['Total Profit (in Rs.)', 'Short Trades'] = self.__getTotalProfit(self.__shorts)
        pnlStats.loc['Total Profit (in Rs.)', 'Long Trades'] = self.__getTotalProfit(self.__longs)
        
        pnlStats.loc['Gross Profit (in Rs.)', 'All Trades'] = self.__getGrossProfit(self.__transactions)
        pnlStats.loc['Gross Profit (in Rs.)', 'Short Trades'] = self.__getGrossProfit(self.__shorts)
        pnlStats.loc['Gross Profit (in Rs.)', 'Long Trades'] = self.__getGrossProfit(self.__longs)
        
        pnlStats.loc['Gross Loss (in Rs.)', 'All Trades'] = self.__getGrossLoss(self.__transactions)
        pnlStats.loc['Gross Loss (in Rs.)', 'Short Trades'] = self.__getGrossLoss(self.__shorts)
        pnlStats.loc['Gross Loss (in Rs.)', 'Long Trades'] = self.__getGrossLoss(self.__longs)

        pnlStats.loc['Profit Factor', 'All Trades'] = self.__getGrossProfit(self.__transactions)/np.abs(self.__getGrossLoss(self.__transactions))
        pnlStats.loc['Profit Factor', 'Short Trades'] = self.__getGrossProfit(self.__shorts)/np.abs(self.__getGrossLoss(self.__shorts))
        pnlStats.loc['Profit Factor', 'Long Trades'] = self.__getGrossProfit(self.__longs)/np.abs(self.__getGrossLoss(self.__longs))
        
        pnlStats.loc['Average Nett. Profit (in Rs.)', 'All Trades'] = self.__getAvgProfit(self.__transactions)
        pnlStats.loc['Average Nett. Profit (in Rs.)', 'Short Trades'] = self.__getAvgProfit(self.__shorts)
        pnlStats.loc['Average Nett. Profit (in Rs.)', 'Long Trades'] = self.__getAvgProfit(self.__longs)
        
        pnlStats.loc['Average Profit (Winning Trades) (in Rs.)', 'All Trades'] = self.__getAvgProfit(self.__transactions, 'Winning')
        pnlStats.loc['Average Profit (Winning Trades) (in Rs.)', 'Short Trades'] = self.__getAvgProfit(self.__shorts, 'Winning')
        pnlStats.loc['Average Profit (Winning Trades) (in Rs.)', 'Long Trades'] = self.__getAvgProfit(self.__longs, 'Winning')
        
        pnlStats.loc['Average Loss (Losing Trades) (in Rs.)', 'All Trades'] = self.__getAvgProfit(self.__transactions, 'Losing')
        pnlStats.loc['Average Loss (Losing Trades) (in Rs.)', 'Short Trades'] = self.__getAvgProfit(self.__shorts, 'Losing')
        pnlStats.loc['Average Loss (Losing Trades) (in Rs.)', 'Long Trades'] = self.__getAvgProfit(self.__longs, 'Losing')
        
        pnlStats.loc['Average Profit:Average Loss', 'All Trades'] = self.__getAvgProfit(self.__transactions, 'Winning')/np.abs(self.__getAvgProfit(self.__transactions, 'Losing'))
        pnlStats.loc['Average Profit:Average Loss', 'Short Trades'] = self.__getAvgProfit(self.__shorts, 'Winning')/np.abs(self.__getAvgProfit(self.__shorts, 'Losing'))
        pnlStats.loc['Average Profit:Average Loss', 'Long Trades'] = self.__getAvgProfit(self.__longs, 'Winning')/np.abs(self.__getAvgProfit(self.__longs, 'Losing'))
        
        pnlStats.loc['Largest Winning Trade (in Rs.)', 'All Trades'] = self.__getLargestTrade(self.__transactions)
        pnlStats.loc['Largest Winning Trade (in Rs.)', 'Short Trades'] = self.__getLargestTrade(self.__shorts)
        pnlStats.loc['Largest Winning Trade (in Rs.)', 'Long Trades'] = self.__getLargestTrade(self.__longs)
        
        pnlStats.loc['Largest Losing Trade (in Rs.)', 'All Trades'] = self.__getLargestTrade(self.__transactions, False)
        pnlStats.loc['Largest Losing Trade (in Rs.)', 'Short Trades'] = self.__getLargestTrade(self.__shorts, False)
        pnlStats.loc['Largest Losing Trade (in Rs.)', 'Long Trades'] = self.__getLargestTrade(self.__longs, False)
        
        pnlStats.columns.name = 'PnL Stats'
        
        return np.round(pnlStats,2)
    
    def __getAvgDuration(self, transactions):
        dur = transactions['Exit Time'] - transactions['Entry Time']
        return np.mean(dur)
    
    def __getMedianDuration(self, transactions):
        sortedDur = (transactions['Exit Time'] - transactions['Entry Time']).sort_values()
        
        mid = int(len(sortedDur)/2)
        if len(sortedDur)%2 == 0:
            mid+=1
        
        return sortedDur.iloc[mid]
    
    def getDurationStats(self):
        durationStats = pd.DataFrame()
        durationStats.loc['Average Duration', 'All Trades'] = self.__getAvgDuration(self.__transactions)
        durationStats.loc['Average Duration', 'Short Trades'] = self.__getAvgDuration(self.__shorts)
        durationStats.loc['Average Duration', 'Long Trades'] = self.__getAvgDuration(self.__longs)
        
        durationStats.loc['Median Duration', 'All Trades'] = self.__getMedianDuration(self.__transactions)
        durationStats.loc['Median Duration', 'Short Trades'] = self.__getMedianDuration(self.__shorts)
        durationStats.loc['Median Duration', 'Long Trades'] = self.__getMedianDuration(self.__longs)
        
        durationStats.columns.name = 'Duration Stats'
        
        return durationStats
    
    def __getAvgReturn(self, transactions, trades='All'):
        
        if trades == 'Winning':
            return np.mean(transactions['Return'].loc[transactions['Return'] > 0])
        
        if trades == 'Losing':
            return np.mean(transactions['Return'].loc[transactions['Return'] < 0])
        
        return np.mean(transactions['Return'])
    
    def __getMedianReturn(self, transactions, trades='All'):
        
        if trades == 'Winning':
            return np.median(transactions['Return'].loc[transactions['Return'] > 0])
        
        if trades == 'Losing':
            return np.median(transactions['Return'].loc[transactions['Return'] < 0])
        
        return np.median(transactions['Return'])
    
    def __getLargestReturn(self, transactions, winning=True):
        
        if not winning:
            return np.min(transactions['Return'].loc[transactions['PnL'] < 0])
        
        return np.max(transactions['Return'].loc[transactions['PnL'] > 0])
    
    def getReturnStats(self):
        returnStats = pd.DataFrame()
        returnStats.loc['Average Returns', 'All Trades'] = self.__getAvgReturn(self.__transactions)
        returnStats.loc['Average Returns', 'Short Trades'] = self.__getAvgReturn(self.__shorts)
        returnStats.loc['Average Returns', 'Long Trades'] = self.__getAvgReturn(self.__longs)
        
        returnStats.loc['Average Returns (Winning Trades)', 'All Trades'] = self.__getAvgReturn(self.__transactions, 'Winning')
        returnStats.loc['Average Returns (Winning Trades)', 'Short Trades'] = self.__getAvgReturn(self.__shorts, 'Winning')
        returnStats.loc['Average Returns (Winning Trades)', 'Long Trades'] = self.__getAvgReturn(self.__longs, 'Winning')
        
        returnStats.loc['Average Returns (Losing Trades)', 'All Trades'] = self.__getAvgReturn(self.__transactions, 'Losing')
        returnStats.loc['Average Returns (Losing Trades)', 'Short Trades'] = self.__getAvgReturn(self.__shorts, 'Losing')
        returnStats.loc['Average Returns (Losing Trades)', 'Long Trades'] = self.__getAvgReturn(self.__longs, 'Losing')
        
        returnStats.loc['Median Returns', 'All Trades'] = self.__getMedianReturn(self.__transactions)
        returnStats.loc['Median Returns', 'Short Trades'] = self.__getMedianReturn(self.__shorts)
        returnStats.loc['Median Returns', 'Long Trades'] = self.__getMedianReturn(self.__longs)
        
        returnStats.loc['Median Returns (Winning Trades)', 'All Trades'] = self.__getMedianReturn(self.__transactions, 'Winning')
        returnStats.loc['Median Returns (Winning Trades)', 'Short Trades'] = self.__getMedianReturn(self.__shorts, 'Winning')
        returnStats.loc['Median Returns (Winning Trades)', 'Long Trades'] = self.__getMedianReturn(self.__longs, 'Winning')
        
        returnStats.loc['Median Returns (Losing Trades)', 'All Trades'] = self.__getMedianReturn(self.__transactions, 'Losing')
        returnStats.loc['Median Returns (Losing Trades)', 'Short Trades'] = self.__getMedianReturn(self.__shorts, 'Losing')
        returnStats.loc['Median Returns (Losing Trades)', 'Long Trades'] = self.__getMedianReturn(self.__longs, 'Losing')
        
        returnStats.loc['Largest Winning Trade Return', 'All Trades'] = self.__getLargestReturn(self.__transactions)
        returnStats.loc['Largest Winning Trade Return', 'Short Trades'] = self.__getLargestReturn(self.__shorts)
        returnStats.loc['Largest Winning Trade Return', 'Long Trades'] = self.__getLargestReturn(self.__longs)
        
        returnStats.loc['Largest Losing Trade Return', 'All Trades'] = self.__getLargestReturn(self.__transactions, False)
        returnStats.loc['Largest Losing Trade Return', 'Short Trades'] = self.__getLargestReturn(self.__shorts, False)
        returnStats.loc['Largest Losing Trade Return', 'Long Trades'] = self.__getLargestReturn(self.__longs, False)
        
        returnStats.columns.name = 'Return Stats (in %)'
        
        return np.round(returnStats*100,2)

    def plotProbProfitTrade(self, ax=None):
        
        x = np.linspace(0, 1., 500)
    
        self.__transactions['profitable'] = self.__transactions.PnL > 0
    
        dist = stats.beta(self.__transactions.profitable.sum(),
                             (~self.__transactions.profitable).sum())
        y = dist.pdf(x)
        lower_perc = dist.ppf(.025)
        upper_perc = dist.ppf(.975)
    
        lower_plot = dist.ppf(.001)
        upper_plot = dist.ppf(.999)
    
        if ax is None:
            ax = plt.subplot()
    
        ax.plot(x, y)
        ax.axvline(lower_perc, color='0.5')
        ax.axvline(upper_perc, color='0.5')
    
        ax.set(xlabel='Probability making a profitable decision', ylabel='Belief',
               xlim=(lower_plot, upper_plot), ylim=(0, y.max() + 1.))
    
        return ax
        
class ReturnsAnalyzer:
    
    def __init__(self, initialCapital, timeframe):
        self.__initialCapital = initialCapital
        self.__timeframe = timeframe
        self.__annFactor = (252*6.25*60/timeframe)
        
    def __findCumRets(self, returns):
        #rets = 1.00 + returns
        return np.cumsum(returns)
    
    def __buildEquity(self, returns):
        equity = self.__initialCapital*np.exp(self.__findCumRets(returns))
        return equity
    
    def plotEquity(self, returns, buyAndHoldReturns=None, ax=None):
        
        if ax is None:
            ax = plt.gca()
        
        eq = self.__buildEquity(returns)
        
        # Build Equity based on buy and hold returns
        buyAndHoldEq = self.__buildEquity(buyAndHoldReturns)
        
        ax.set(xlabel='', ylabel='Equity')
        
        eq.plot(lw=3, color='forestgreen', alpha=0.6, label='Backtest', ax=ax)
        buyAndHoldEq.plot(lw=2, color='gray', label='Buy And Hold', alpha=0.60, ax=ax)
        
        ax.legend(loc='best')
        ax.axhline(self.__initialCapital, linestyle='--', color='black', lw=2)
        ax.set_title('Equity')
        return ax
    
    def plotUnderwaterCurve(self, returns, ax=None):
        if ax is None:
            ax = plt.gca()
         
        eq = self.__buildEquity(returns)
        
        running_max = np.maximum.accumulate(eq)
        underwater = -100 * ((running_max - eq) / running_max)
        (underwater).plot(ax=ax, kind='area', color='coral', alpha=0.7)
        ax.set_ylabel('Drawdown')
        ax.set_title('Underwater Curve')
        ax.set_xlabel('')
        return ax
    
    def __getMaxDrawdownUnderwater(self, underwater):
    
        valley = np.argmin(underwater)
        peak = underwater[:valley][underwater[:valley] == 0].index[-1]
        try:
            recovery = underwater[valley:][underwater[valley:] == 0].index[0]
        except IndexError:
            recovery = np.nan
        return peak, valley, recovery
    
    def __getTopDrawdowns(self, returns, top=10):
        
        equity = self.__buildEquity(returns)
        running_max = np.maximum.accumulate(equity)
        underwater = equity / running_max - 1
    
        drawdowns = []
        for t in range(top):
            peak, valley, recovery = self.__getMaxDrawdownUnderwater(underwater)
            # Slice out draw-down period
            if not pd.isnull(recovery):
                underwater.drop(underwater[peak: recovery].index[1:-1],
                                inplace=True)
            else:
                # drawdown has not ended yet
                underwater = underwater.loc[:peak]
    
            drawdowns.append((peak, valley, recovery))
            if (len(returns) == 0) or (len(underwater) == 0):
                break
    
        return drawdowns
    
    def __genDrawdownTable(self, returns, top=10):
        
        equity = self.__buildEquity(returns)
        drawdownPeriods = self.__getTopDrawdowns(returns, top=top)
        dfDrawdowns = pd.DataFrame(index=list(range(top)),
                                    columns=['net drawdown in %',
                                             'peak date',
                                             'valley date',
                                             'recovery date',
                                             'duration'])
    
        for i, (peak, valley, recovery) in enumerate(drawdownPeriods):
            if pd.isnull(recovery):
                dfDrawdowns.loc[i, 'duration'] = np.nan
            else:
                dfDrawdowns.loc[i, 'duration'] = len(pd.date_range(peak,
                                                                    recovery,
                                                                    freq='B'))
            dfDrawdowns.loc[i, 'peak date'] = (peak.to_pydatetime()
                                                .strftime('%Y-%m-%d'))
            dfDrawdowns.loc[i, 'valley date'] = (valley.to_pydatetime()
                                                  .strftime('%Y-%m-%d'))
            if isinstance(recovery, float):
                dfDrawdowns.loc[i, 'recovery date'] = recovery
            else:
                dfDrawdowns.loc[i, 'recovery date'] = (recovery.to_pydatetime()
                                                        .strftime('%Y-%m-%d'))
            dfDrawdowns.loc[i, 'net drawdown in %'] = np.round( (
                (equity.loc[peak] - equity.loc[valley]) / equity.loc[peak]) * 100, 2)
    
        dfDrawdowns['peak date'] = pd.to_datetime(dfDrawdowns['peak date'])
        dfDrawdowns['valley date'] = pd.to_datetime(dfDrawdowns['valley date'])
        dfDrawdowns['recovery date'] = pd.to_datetime(dfDrawdowns['recovery date'])
    
        return dfDrawdowns
    
    def plotDrawdownPeriods(self, returns, top=5, ax=None):
        
        if ax is None:
            ax = plt.gca()
        
        equity = self.__buildEquity(returns)
        dfDrawdowns = self.__genDrawdownTable(returns, top=top)
    
        equity.plot(ax=ax)
    
        lim = ax.get_ylim()
        colors = sns.cubehelix_palette(len(dfDrawdowns))[::-1]
        for i, (peak, recovery) in dfDrawdowns[
                ['peak date', 'recovery date']].iterrows():
            if pd.isnull(recovery):
                recovery = returns.index[-1]
            ax.fill_between((peak, recovery),
                            lim[0],
                            lim[1],
                            alpha=.4,
                            color=colors[i])
    
        ax.set_title('Top %i Drawdown Periods' % top)
        ax.set_ylabel('Equity')
        ax.legend(['Equity'], loc='upper left')
        ax.set_xlabel('')
        return ax
    
    def __getRollingSharpe(self, returns, window):
        
        return returns.rolling(window).mean() \
            / returns.rolling(window).std() \
            * np.sqrt(252*6.25*60/self.__timeframe)
    
    def plotRollingSharpe(self, returns, window=252, ax=None):
        
        if ax is None:
            ax = plt.gca()
        
        
        
        window = int((252*6.25*60/self.__timeframe)/2)
        
        rollingSharpe = self.__getRollingSharpe(returns, window)
        
        rollingSharpe.plot(alpha=.7, lw=3, color='orangered', ax=ax)
    
        ax.set_title('Rolling Sharpe ratio (6-month)')
        ax.axhline(
            rollingSharpe.mean(),
            color='steelblue',
            linestyle='--',
            lw=3)
        ax.axhline(0.0, color='black', linestyle='-', lw=3)
    
        ax.set_ylim((-3.0, 6.0))
        ax.set_ylabel('Sharpe ratio')
        ax.set_xlabel('')
        ax.legend(['Sharpe', 'Average'], loc='best')
        return ax
    
    def __getMonthlyReturns(self, returns):
        
        def getReturns(x):
            return (np.exp(self.__findCumRets(x))[-1]-1.0)*100.0
        
        return returns.groupby([lambda x: x.year, lambda x: x.month]).apply(getReturns)
    
    def __getAnnualReturns(self, returns):
        
        def getReturns(x):
            return (np.exp(self.__findCumRets(x))[-1]-1.0)*100.0
        
        return returns.groupby([lambda x: x.year]).apply(getReturns)
    
    def plotMonthlyReturnsHeatmap(self, returns, ax=None):
        if ax is None:
            ax = plt.gca()
    
        monthlyRetTable = self.__getMonthlyReturns(returns)
        monthlyRetTable = monthlyRetTable.unstack()
        
        monthlyRetTable = monthlyRetTable.fillna(0.0)
        monthlyRetTable = np.round(monthlyRetTable,1)
        
        sns.heatmap(
            monthlyRetTable,
            annot=True,
            annot_kws={
                "size": 8},
            fmt="",
            cbar=False,
            cmap=matplotlib.cm.RdYlGn,
            ax=ax)
        
        ax.set_ylabel('Year')
        ax.set_xlabel('Month')
        ax.set_title("Monthly Returns (%)")
        return ax
    
    def plotAnnualReturns(self, returns, ax=None):
        if ax is None:
            ax = plt.gca()
    
        annRetDf = self.__getAnnualReturns(returns)
    
        ax.axvline(
            annRetDf.values.mean(),
            color='steelblue',
            linestyle='--',
            lw=4,
            alpha=0.7)
        
        (annRetDf.sort_index(ascending=False)).plot(ax=ax, 
                                                      kind='barh', 
                                                      alpha=0.70,
                                                      color='steelblue')
        ax.axvline(0.0, color='black', linestyle='-', lw=3)
    
        ax.set_ylabel('Year')
        ax.set_xlabel('Returns')
        ax.set_title("Annual Returns")
        ax.legend(['mean'])
        return ax
    
    def plotMonthlyReturnsDist(self, returns, ax=None):
        if ax is None:
            ax = plt.gca()
    
        monthlyRetTable = self.__getMonthlyReturns(returns)
    
        ax.hist(
            monthlyRetTable,
            color='orangered',
            alpha=0.80,
            bins=20)
    
        ax.axvline(
            monthlyRetTable.mean(),
            color='gold',
            linestyle='--',
            lw=4,
            alpha=1.0)
    
        ax.axvline(0.0, color='black', linestyle='-', lw=3, alpha=0.75)
        ax.legend(['mean'])
        ax.set_ylabel('Number of months')
        ax.set_xlabel('Returns')
        ax.set_title("Distribution of Monthly Returns")
        return ax
    
    def annual_return(self, returns):
        
        if len(returns) < 1:
            return np.nan
        
        numYears = float(len(returns)) / self.__annFactor
        eq = self.__buildEquity(returns)
        startValue = eq.iloc[0]
        endValue = eq.iloc[-1]
    
        totalReturn = (endValue - startValue) / startValue
        annRet = (1. + totalReturn) ** (1 / numYears) - 1
    
        return annRet*100
    
    def annual_volatility(self, returns):
        if len(returns) < 2:
            return np.nan
    
        return returns.std() * np.sqrt(self.__annFactor)*100
    
    def sharpe_ratio(self, returns):
        
        if (len(returns) < 5) or np.all(returns == 0):
            return np.nan
        
        sharpe = (np.mean(returns) / np.std(returns)) * np.sqrt(self.__annFactor)
        return sharpe
    
    def calmar_ratio(self, returns):
        
        tempMaxDD = self.max_drawdown(returns)
        if tempMaxDD < 0:
            temp = self.annual_return(returns) / abs(self.max_drawdown(returns))
        else:
            return np.nan
    
        if np.isinf(temp):
            return np.nan
    
        return temp
    
    def max_drawdown(self, returns):
        if returns.size < 1:
            return np.nan
    
        eq = self.__buildEquity(returns)
        cumMaxEq = eq.cummax()
    
        return eq.sub(cumMaxEq).div(cumMaxEq).min()*100
    
    def stability_of_timeseries(self, returns):
        cumLogReturns = np.log1p(returns).cumsum()
        rhat = stats.linregress(np.arange(len(cumLogReturns)),
                                cumLogReturns.values)[2]
    
        return rhat
    
    def omega_ratio(self, returns, annualReturnThreshhold=0.0):
        
        dailyReturnThresh = pow(1 + annualReturnThreshhold, 1 / self.__annFactor) - 1
    
        returnsLessThresh = returns - dailyReturnThresh
    
        numer = sum(returnsLessThresh[returnsLessThresh > 0.0])
        denom = -1.0 * sum(returnsLessThresh[returnsLessThresh < 0.0])
    
        if denom > 0.0:
            return numer / denom
        else:
            return np.nan
    
    def sortino_ratio(self, returns):
        
        if (len(returns) < 5) or np.all(returns == 0):
            return np.nan
        
        sortino = (np.mean(returns) / np.std(returns[returns < 0])) * np.sqrt(self.__annFactor)
        return sortino
    
    def tail_ratio(self, returns):
        return np.abs(np.percentile(returns, 95)) / np.abs(np.percentile(returns, 5))
    
    def common_sense_ratio(self, returns):
        return self.tail_ratio(returns) * (1 + self.annual_return(returns))
    
    def information_ratio(self, returns, buyAndHoldReturns):
        activeReturn = returns - buyAndHoldReturns
        trackingError = np.std(activeReturn, ddof=1)
        if np.isnan(trackingError):
            return 0.0
        return np.mean(activeReturn) / trackingError
    
    
    def alpha_beta(self, returns, buyAndHoldReturns):
        
        retIndex = returns.index
        beta, alpha = stats.linregress(buyAndHoldReturns.loc[retIndex].values,
                                          returns.values)[:2]
    
        return alpha * self.__annFactor, beta
    
    
    def alpha(self, returns, buyAndHoldReturns):
        return self.alpha_beta(returns, buyAndHoldReturns)[0]
    
    
    def beta(self, returns, buyAndHoldReturns):
        return self.alpha_beta(returns, buyAndHoldReturns)[1]
    
    def calcPerfStats(self, returns, buyAndHoldReturns):
        
        SIMPLE_STAT_FUNCS = [
            self.annual_return,
            self.annual_volatility,
            self.sharpe_ratio,
            self.calmar_ratio,
            #self.stability_of_timeseries,
            self.max_drawdown,
            self.omega_ratio,
            self.sortino_ratio,
            stats.skew,
            stats.kurtosis,
            self.tail_ratio
            #self.common_sense_ratio
        ]
        
        FACTOR_STAT_FUNCS = [
            #self.information_ratio,
            self.alpha,
            self.beta,
        ]
        
        perfStats = pd.Series()
    
        for statFunc in SIMPLE_STAT_FUNCS:
            if statFunc.__name__ in ['annual_return','annual_volatility','max_drawdown']:
                name = statFunc.__name__+' (%)'
                perfStats[name] = statFunc(returns)
            else:
                perfStats[statFunc.__name__] = statFunc(returns)
        
        if buyAndHoldReturns is not None:
            for statFunc in FACTOR_STAT_FUNCS:
                perfStats[statFunc.__name__] = statFunc(returns,
                                                      buyAndHoldReturns)
        
        #perfStats = perfStats
        perfStats =  pd.DataFrame(perfStats, columns=['Backtest'])
        perfStats.columns.name = 'Performance Statistics'
        display(perfStats)
    
    def showWorstDrawdowns(self, returns, top=5):
        dfDrawdowns = self.__genDrawdownTable(returns, top=top)
        dfDrawdowns.columns.name = 'Worst Drawdown Periods'
        
        display(dfDrawdowns.sort_values('net drawdown in %', ascending=False))