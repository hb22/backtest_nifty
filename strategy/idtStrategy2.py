#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 21:25:21 2018

@author: himanshu
"""

import numpy as np
import pandas as pd
import talib as ta
import os
import itertools
import datetime as dt

from strategy import Strategy

class IDTStrategy2(Strategy):
    
    def __init__(self, symbol, initCap, lots=1, commission=5.00, startYr=None, endYr=None):
        super().__init__()
        
        self.__symbol = symbol
        self.__initCap = initCap
        self.__startYr = startYr
        self.__endYr = endYr
        self.__lots = lots
        self.__commission = commission
        
    def run(self, combo, getDailyReturns=False):
        
        print('Combo', combo, self.__startYr, self.__endYr)
        
        timeframe = combo[0]
        data = pd.read_csv('data'+os.sep+str(timeframe)+'_mins'+os.sep+self.__symbol+'.csv')
        #del data['Up']
        #del data['Down']
        
        if (self.__startYr is not None) and (self.__endYr is not None):
            data['Year'] = data['Date'].str.split('/').apply(lambda x:x[2])
            data['Year'] = pd.to_numeric(data['Year'])
            data = data.loc[(data['Year'] >= self.__startYr) & (data['Year'] <= self.__endYr)]
            del data['Year']
        
        data['Symbol'] = self.__symbol
        symbolDetails = pd.read_csv('symbol_details.csv')
        Point_Value = int(symbolDetails['Point Value'].loc[symbolDetails['Symbol']==self.__symbol])*self.__lots
        Ticks_per_Point = 1.0/(float(symbolDetails['Tick Size'].loc[symbolDetails['Symbol']==self.__symbol]))
        Initial_Capital = self.__initCap
        CommXtrade = self.__commission*self.__lots
        
        EMA1 = combo[1]
        EMA2 = combo[2]
        EMA3 = combo[3]
        EMA4 = combo[4]
        TPLength = combo[5]
        LengthAVGP100 = combo[6]
        
        LengthDKHL = combo[7]
        LengthDKLL = combo[8]
        ProfTriggATR = combo[9]
        Stop100ATR = combo[10]
        FuerzaDesvL = combo[11]
        FuerzaDesvC = combo[12]
        atrLen = combo[13]

        data['EMA1'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA1)   # Exponential MA1
        data['EMA2'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA2)   # Exopnential MA2
        data['EMA3'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA3)   # Exopnential MA3
        data['EMA4'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA4)   # Exopnential MA4
        
        # Calculating MA ratios Dif variable will be the difference between two Close/EMA Ratios
        data['DifPL'] = (data['Close']/data['EMA4'] - 1)*100        # Close/LongTerm_MA ratio in Pct values
        data['DifPC'] = (data['Close']/data['EMA1'] - 1)*100        # Close/ShortTerm_MA ratio in Pct values
        data['DifP100'] = data['DifPL'] - data['DifPC']      # Dif of MA ratios LongTerm - ShortTerm will define MAratiosDif variable
        data['Max'] = data['DifP100'].rolling(TPLength).max()
        data['Min'] = data['DifP100'].rolling(TPLength).min()
        data['Promedio'] = data['DifP100'].rolling(LengthAVGP100).mean()
        data['TrendPower100'] = np.zeros(len(data))
        data.loc[data['Promedio']>0, 'TrendPower100'] = (data['Promedio']*100)/(data['Max']+0.01)                    
        data.loc[data['Promedio']<0, 'TrendPower100'] = (data['Promedio']*100)/((data['Min']*-1)+0.01)
        
        # Calculating MAtrend variable, MAtrend will take values of 0,5 or 10, will compute the intensity depending on the 4 EMA levels
        data['TrendValue'] = np.zeros(len(data))                            
        data.loc[(data['EMA3']>data['EMA4']), 'TrendValue'] = 1                    
        data.loc[(data['EMA3']<data['EMA4']), 'TrendValue'] = 0             
        
        # Calculating Donchian Channels
        data['Highline'] = data['High'].rolling(LengthDKHL).max().shift(1)       #Upper Band - Donchian Breakout
        data['Lowline'] = data['Low'].rolling(LengthDKLL).min().shift(1)         #Lower Band - Donchian Breakout 
        
        data['ATR'] = ta.ATR(np.asarray(data['High']),np.asarray(data['Low']),np.asarray(data['Close']), timeperiod = atrLen)
        # Calculating HvsC and LvsC variables for Longs and Shorts Entries respectively
        data['HvsC_ratio'] = np.zeros(len(data))                       # Highs vs Close distance for Long Entries
        data.loc[((data['High']-data['Open'])>0)&((data['Close']-data['Open'])>0), 'HvsC_ratio'] = (data['Close']-data['Open'])/(data['High']-data['Open']) # Pct of the range of the bar 
        data['LvsC_ratio'] = np.zeros(len(data))
        data.loc[((data['Open']-data['Low'])>0)&((data['Open']-data['Close'])>0), 'LvsC_ratio'] = (data['Open']-data['Close'])/(data['Open']-data['Low'])
        
        # For Calculating the Trend Filter, the 3 last variables will be used to build a signal of: 1, 0 or -1 to allow or not Entry Rule execution   
        data['TrendFilter'] = np.zeros(len(data))
        data.loc[((data['TrendValue']>0)&(data['HvsC_ratio']>0.675)&(data['TrendPower100']>FuerzaDesvL)), 'TrendFilter'] = 1
        data.loc[((data['TrendValue']<1)&(data['LvsC_ratio']>0.675)&(data['TrendPower100']<FuerzaDesvC)), 'TrendFilter'] = -1
        
        # Creating columns for signals, positions and MtoM PnL etc
        data['Buy_Signal'] = np.zeros(len(data))                # Buy_Signal = 1 means Bought
        data['Sell_Signal'] = np.zeros(len(data))               # Sell_Signal = -1 mean Sold
        data['MktPos'] = np.zeros(len(data))                    # Will be 1 for Longs, 0 Flat, -1 for Shorts
        data['EntryPrice'] = np.zeros(len(data))                # Will save Entry Price when occurs for Longs or Shorts 
        data['ExitPrice'] = np.zeros(len(data))                 # Will save Exit Price when occurs for Longs or Shorts
        data['ExitRule'] = ''                                   # Will save a string describing the Exit Rule used for each trade at the end
        data['MtoM'] = np.zeros(len(data))                      # Mark to Market will compute current cumultive trade P&L in price point values
        data['MtoM_Chg'] = np.zeros(len(data))                  # Compute the Bar to Bar change of MtoM
        data['Daily_P&L'] = np.zeros(len(data))                 # USD P&L multipliying MtoM Chg by the point value (on 1 contract)
        data['Trade_Cost'] = np.zeros(len(data))                # Trading Costs divided /2 1st half at entry 2nd at exit 
        data['D_Net_P&L'] = np.zeros(len(data))                 # Daily P&L after Trading Costs
        data['Longs'] = np.zeros(len(data))                     # Will count Long trades at the end
        data['Shorts'] = np.zeros(len(data))                    # Will count Short trades at the end
        data['Trade_PnL'] = np.zeros(len(data))                 # Compute total USD P&L at the end of the trade
        data['MaxMtM'] = np.zeros(len(data))                    # Max of MtoM per trade will be used to compute when some Pct of profit is achieved
        data['BestClose'] = data['Close']                       # Best Close price represents max P&L achieved for Longs and Shorts trades used for TE Rule
        data['Log_Return'] = np.zeros(len(data))                # Use MtoM to compute Ln Returns for each bar used to compute Sharpe Ratio
        data['Trade_Return'] = np.zeros(len(data))              # Simple Return at the end of each trade calculated when Exit occurs 
        data['Equity'] = np.ones(len(data))*Initial_Capital
        data = data.dropna()
        
        N = len(data)
        data.index = range(0,N)
        for i in range(1,N):
            time = dt.datetime.strptime(data['Time'][i], '%H:%M:%S').time()
            ################################################ IF WE ARE FLAT THEN... #################################################
            if (data['MktPos'][i-1] == 0):
                data.at[data.index[i-1],'BestClose']= 0
                ##################################### START ENTRY RULE FOR LONGS #######################################
                if (time < dt.time(15,0,0)) and ((data['Close'][i-1] > data['Highline'][i-1]) and (data['TrendFilter'][i-1] > 0)):   
                    entrybar = i
                    data.at[data.index[i],'Buy_Signal']= 1
                    entryprice = data['Open'][i]
                    data.at[data.index[i],'EntryPrice']= entryprice
                    data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                    Stop_Price = round((entryprice-(data['ATR'][i-1]*Stop100ATR))*Ticks_per_Point)/Ticks_per_Point  # Rounding to get a real Future Price with Ticks
                    if (data['Low'][i] < Stop_Price):                # This If condition is for Stop Losses on Entry bar "bad entries"
                        data.at[data.index[i],'Sell_Signal']= -1
                        data.at[data.index[i],'Longs']= 1
                        data.at[data.index[i],'MktPos']= 0
                        exitprice = Stop_Price
                        data.at[data.index[i],'ExitPrice']= exitprice
                        data.at[data.index[i],'ExitRule']= 'Stop-Loss'
                        data.at[data.index[i],'BestClose']= 0
                        data.at[data.index[i],'MtoM']= (exitprice - entryprice)
                        data.at[data.index[i],'MtoM_Chg']= data['MtoM'][i]
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'Trade_Cost']= -CommXtrade
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                        InvestedEq = data['Equity'][i-1]
                        ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                        data.at[data.index[i],'Trade_Return']= ret
                        data.at[data.index[i],'Equity']= (InvestedEq + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                    else:
                        data.at[data.index[i],'MktPos']= 1
                        data.at[data.index[i],'BestClose']= (data['Close'][entrybar:(i+1)].max())
                        data.at[data.index[i],'MtoM']= (data['Close'][i] - entryprice)
                        data.at[data.index[i],'MtoM_Chg']= data['MtoM'][i]
                        data.at[data.index[i],'MaxMtM']= data['MtoM'][entrybar:(i+1)].max()
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        InvestedEq = data['Equity'][i-1]
                        data.at[data.index[i],'Equity']= (InvestedEq + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                ##################################### START ENTRY RULE FOR SHORTS #######################################
                elif (time < dt.time(15,0,0)) and ((data['Close'][i-1] < data['Lowline'][i-1]) and (data['TrendFilter'][i-1] < 0)): 
                    entrybar = i
                    data.at[data.index[i],'Sell_Signal']= -1
                    entryprice = data['Open'][i]
                    data.at[data.index[i],'EntryPrice']= entryprice
                    data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                    Stop_Price = round((entryprice+(data['ATR'][i-1]*Stop100ATR))*Ticks_per_Point)/Ticks_per_Point
                    if (data['High'][i] > Stop_Price):
                        data.at[data.index[i],'Buy_Signal']= 1
                        data.at[data.index[i],'Shorts']= 1
                        data.at[data.index[i],'MktPos']= 0
                        exitprice = Stop_Price
                        data.at[data.index[i],'ExitPrice']= exitprice
                        data.at[data.index[i],'ExitRule']= 'Stop-Loss'
                        data.at[data.index[i],'BestClose']= 0
                        data.at[data.index[i],'MtoM']= (entryprice - exitprice)
                        data.at[data.index[i],'MtoM_Chg']= data['MtoM'][i]
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'Trade_Cost']= -CommXtrade
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                        InvestedEq = data['Equity'][i-1]
                        ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                        data.at[data.index[i],'Trade_Return']= ret
                        data.at[data.index[i],'Equity']= (InvestedEq + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                    else:
                        data.at[data.index[i],'MktPos']= -1
                        data.at[data.index[i],'BestClose']= (data['Close'][entrybar:(i+1)].min())
                        data.at[data.index[i],'MtoM']= (entryprice - data['Close'][i])
                        data.at[data.index[i],'MtoM_Chg']= data['MtoM'][i]
                        data.at[data.index[i],'MaxMtM']= (data['MtoM'][entrybar:(i+1)].max())
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        InvestedEq = data['Equity'][i-1]
                        data.at[data.index[i],'Equity']= (InvestedEq + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                else:
                    data.at[data.index[i],'Equity']= data['Equity'][i-1]           # If there is no trade then Equity remains at the same value
            ################################################### IF WE ARE LONG THEN... ###########################################
            elif (data['MktPos'][i-1] == 1):
                if time == dt.time(15,0,0):
                    data.at[data.index[i],'Sell_Signal']= -1
                    data.at[data.index[i],'Longs']= 1                               
                    data.at[data.index[i],'MktPos']= 0
                    exitprice = round(((0.75*(data['BestClose'][i-1]-entryprice))+entryprice)*Ticks_per_Point)/Ticks_per_Point
                    data.at[data.index[i],'ExitPrice']= exitprice
                    data.at[data.index[i],'MtoM']= 0
                    data.at[data.index[i],'MtoM_Chg']= (exitprice - data['Close'][i-1])
                    data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                    data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                    data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                    data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                    data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                    data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                    ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                    data.at[data.index[i],'Trade_Return']= ret
                    data.at[data.index[i],'ExitRule']= 'Time_Xt'
                elif (data['BestClose'][i-1] > (entryprice+(ProfTriggATR*data['ATR'][entrybar]))):
                    ################################################### TRAILING EXIT  ###########################################
                    if (data['Low'][i] < ((0.75*(data['BestClose'][i-1]-entryprice))+entryprice)):
                        data.at[data.index[i],'Sell_Signal']= -1
                        data.at[data.index[i],'Longs']= 1                               
                        data.at[data.index[i],'MktPos']= 0
                        exitprice = round(((0.75*(data['BestClose'][i-1]-entryprice))+entryprice)*Ticks_per_Point)/Ticks_per_Point
                        data.at[data.index[i],'ExitPrice']= exitprice
                        data.at[data.index[i],'MtoM']= 0
                        data.at[data.index[i],'MtoM_Chg']= (exitprice - data['Close'][i-1])
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                        ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                        data.at[data.index[i],'Trade_Return']= ret
                        data.at[data.index[i],'ExitRule']= 'Trailing_Xt'
                    else:
                        data.at[data.index[i],'MktPos']= 1                           
                        data.at[data.index[i],'MtoM']= (data['Close'][i] - entryprice)
                        data.at[data.index[i],'MtoM_Chg']= (data['MtoM'][i]-data['MtoM'][i-1])
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'MaxMtM']= (data['MtoM'][entrybar:(i+1)].max())
                        data.at[data.index[i],'BestClose']= (data['Close'][entrybar:(i+1)].max())
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                elif (data['BestClose'][i-1] < (entryprice+(ProfTriggATR*data['ATR'][entrybar]))):
                    ################################################### STOP LOSS  ###########################################
                    if (data['Low'][i] < Stop_Price):     
                        data.at[data.index[i],'Sell_Signal']= -1       # If Trailing exit condition never occurs then we wait until the Protective Stop level
                        data.at[data.index[i],'Longs']= 1
                        data.at[data.index[i],'MktPos']= 0
                        exitprice = Stop_Price
                        data.at[data.index[i],'ExitPrice']= exitprice
                        data.at[data.index[i],'MtoM']= 0
                        data.at[data.index[i],'MtoM_Chg']= (exitprice - data['Close'][i-1])
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                        ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                        data.at[data.index[i],'Trade_Return']= ret
                        data.at[data.index[i],'ExitRule']= 'Stop-Loss'
                    else:
                        data.at[data.index[i],'MktPos']= 1                           
                        data.at[data.index[i],'MtoM']= (data['Close'][i] - entryprice)
                        data.at[data.index[i],'MtoM_Chg']= (data['MtoM'][i]-data['MtoM'][i-1])
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'MaxMtM']= (data['MtoM'][entrybar:(i+1)].max())
                        data.at[data.index[i],'BestClose']= (data['Close'][entrybar:(i+1)].max())
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
            ################################################### IF WE ARE SHORT THEN... ###########################################
            elif (data['MktPos'][i-1] == -1):
                if time == dt.time(15,0,0):
                    data.at[data.index[i],'Buy_Signal']= 1
                    data.at[data.index[i],'Shorts']= 1
                    data.at[data.index[i],'MktPos']= 0
                    exitprice = round((entryprice - (0.75*(entryprice - data['BestClose'][i-1])))*Ticks_per_Point)/Ticks_per_Point
                    data.at[data.index[i],'ExitPrice']= exitprice
                    data.at[data.index[i],'MtoM']= 0
                    data.at[data.index[i],'MtoM_Chg']= (data['Close'][i-1] - exitprice)
                    data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                    data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                    data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                    data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                    data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                    data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                    ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                    data.at[data.index[i],'Trade_Return']= ret
                    data.at[data.index[i],'ExitRule']= 'Time_Xt'
                if (data['BestClose'][i-1] < (entryprice-(ProfTriggATR*data['ATR'][entrybar]))):
                    ################################################### TRAILING EXIT  ###########################################
                    if (data['High'][i] > (entryprice - (0.75*(entryprice - data['BestClose'][i-1])))):
                        data.at[data.index[i],'Buy_Signal']= 1
                        data.at[data.index[i],'Shorts']= 1
                        data.at[data.index[i],'MktPos']= 0
                        exitprice = round((entryprice - (0.75*(entryprice - data['BestClose'][i-1])))*Ticks_per_Point)/Ticks_per_Point
                        data.at[data.index[i],'ExitPrice']= exitprice
                        data.at[data.index[i],'MtoM']= 0
                        data.at[data.index[i],'MtoM_Chg']= (data['Close'][i-1] - exitprice)
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                        ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                        data.at[data.index[i],'Trade_Return']= ret
                        data.at[data.index[i],'ExitRule']= 'Trailing_Xt'
                    else:
                        data.at[data.index[i],'MktPos']= -1
                        data.at[data.index[i],'MtoM']= (entryprice-data['Close'][i])
                        data.at[data.index[i],'MtoM_Chg']= (data['MtoM'][i]-data['MtoM'][i-1])
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'MaxMtM']= data['MtoM'][entrybar:(i+1)].max()
                        data.at[data.index[i],'BestClose']= data['Close'][entrybar:(i+1)].min()
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                elif (data['BestClose'][i-1] > (entryprice-(ProfTriggATR*data['ATR'][entrybar]))):
                    ################################################### STOP LOSS ###########################################
                    if (data['High'][i] > Stop_Price):
                        data.at[data.index[i],'Buy_Signal']= 1
                        data.at[data.index[i],'Shorts']= 1
                        data.at[data.index[i],'MktPos']= 0
                        exitprice = Stop_Price
                        data.at[data.index[i],'ExitPrice']= exitprice
                        data.at[data.index[i],'MtoM']= 0
                        data.at[data.index[i],'MtoM_Chg']= (data['Close'][i-1] - exitprice)
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'Trade_Cost']= -CommXtrade/2
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.at[data.index[i],'Trade_PnL']= (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-CommXtrade
                        ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                        data.at[data.index[i],'Trade_Return']= ret
                        data.at[data.index[i],'ExitRule']= 'Stop-Loss'
                    else:
                        data.at[data.index[i],'MktPos']= -1
                        data.at[data.index[i],'MtoM']= (entryprice-data['Close'][i])
                        data.at[data.index[i],'MtoM_Chg']= (data['MtoM'][i]-data['MtoM'][i-1])
                        data.at[data.index[i],'Daily_P&L']= (data['MtoM_Chg'][i]*Point_Value)
                        data.at[data.index[i],'D_Net_P&L']= (data['Daily_P&L'][i]+data['Trade_Cost'][i])
                        data.at[data.index[i],'MaxMtM']= data['MtoM'][entrybar:(i+1)].max()
                        data.at[data.index[i],'BestClose']= data['Close'][entrybar:(i+1)].min()
                        data.at[data.index[i],'Equity']= (data['Equity'][i-1] + data['D_Net_P&L'][i])
                        data.at[data.index[i],'Log_Return']= (np.log(data['Equity'][i]/data['Equity'][i-1]))        
    
        if getDailyReturns:
            
            columnsReqd = ['Date', 
                           'Time', 
                           'Symbol', 
                           'Close',
                           'EntryPrice', 
                           'ExitPrice', 
                           'Longs', 
                           'Shorts',
                           'Equity'
                          ]
            analyzerData = data[columnsReqd]
            return (self.getReturns(analyzerData), 
                    self.getBuyAndHoldReturns(analyzerData), 
                    self.getTransactions(analyzerData, Point_Value)
                    )
            
            
        df = self.calcStats(timeframe, data, self.__initCap, combo)
        return df
        
    def combosGenerator(self):
        timeframe = [15]
        
        ema1 = [7]
        ema2 = [12]
        ema3 = [25]
        ema4 = [50]
        
        tpLength = [50]
        lengthAVGP100 = [25]
        
        lengthDKHL = [180,240,300]
        lengthDKLL = [180,240,300]
        profTriggATR = [2.0,3.0]
        stop100ATR = [1.5,1.75,2.0]
        fuerzaDesvL = [3,4,5]
        fuerzaDesvC = [-3,-4,-5]
        atrLen = [400, 420,450]
        
        return list(itertools.product(timeframe, ema1, ema2, ema3, ema4, 
                                      tpLength, lengthAVGP100, lengthDKHL, 
                                      lengthDKLL, profTriggATR, stop100ATR,
                                      fuerzaDesvL, fuerzaDesvC, atrLen))
    
    