#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  5 16:51:18 2018

@author: himanshu
"""

import numpy as np
import talib as ta
import pandas as pd
import os
import itertools
import datetime as dt

from strategy import Strategy

class IDTStrategy(Strategy):
    
    def __init__(self, symbol, initCap, startYr=None, endYr=None):
        super().__init__()
        
        self.__symbol = symbol
        self.__initCap = initCap
        self.__startYr = startYr
        self.__endYr = endYr
        self.__tradeCost = 200.00
        
    def run(self, combo, getDailyReturns=False):
        
        print('Combo', combo)
        
        timeframe = combo[0]
        data = pd.read_csv('data'+os.sep+str(timeframe)+'_mins'+os.sep+self.__symbol+'.csv')
        #del data['Up']
        #del data['Down']
        
        if (self.__startYr is not None) and (self.__endYr is not None):
            data['Year'] = data['Date'].str.split('/').apply(lambda x:x[2])
            data['Year'] = pd.to_numeric(data['Year'])
            data = data.loc[(data['Year'] >= self.__startYr) & (data['Year'] <= self.__endYr)]
            del data['Year']
        
        data['Symbol'] = self.__symbol
        symbolDetails = pd.read_csv('symbol_details.csv')
        Point_Value = int(symbolDetails['Point Value'].loc[symbolDetails['Symbol']==self.__symbol])
        Ticks_per_Point = 1.0/(float(symbolDetails['Tick Size'].loc[symbolDetails['Symbol']==self.__symbol]))
        Initial_Capital = self.__initCap  #25,000 USD Account
        
        EMA1 = combo[1]            #Exponential MA1 period
        EMA2 = combo[2]        #Exponential MA2 period
        EMA3 = combo[3]         #Exponential MA3 period
        EMA4 = combo[4]         #Exponential MA4 period
        #########################################################################################################################################
        ############################################################ Input Parameteres ##########################################################
        DnchnUp = combo[5]
        DnchnDn = combo[6]
        
        ProfTriggLE = combo[7]
        StpPctLE = combo[8]
        StrengthLE = combo[9]
        
        ProfTriggSE = combo[10]
        StpPctSE = combo[11]
        StrengthSE = combo[12]
        #########################################################################################################################################
        
        # Calculating Exponential MAs using TA-Lib, EMAs will be used to calculate Trend Variables used to build the Trend Filter
        data['EMA1'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA1)   # Exponential MA1
        data['EMA2'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA2)   # Exopnential MA2
        data['EMA3'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA3)   # Exopnential MA3
        data['EMA4'] = ta.EMA(np.asarray(data['Close']), timeperiod = EMA4)   # Exopnential MA4
        
        # Calculating MAratiosDif variable will be the difference between two Close/EMA Ratios
        data['MA4ratio'] = (data['Close']/data['EMA4'] - 1)*100        # Close/LongTerm_MA ratio in Pct values
        data['MA1ratio'] = (data['Close']/data['EMA1'] - 1)*100        # Close/ShortTerm_MA ratio in Pct values
        data['MAratiosDif'] = data['MA4ratio'] - data['MA1ratio']      # Dif of MA ratios LongTerm - ShortTerm will define MAratiosDif variable
        
        # Calculating MAtrend variable, MAtrend will take values of 0,5 or 10, will compute the intensity depending on the 4 EMA levels
        data['MAtrend'] = np.zeros(len(data))                            
        data.loc[(data['EMA2']>data['EMA3'])&(data['EMA3']>data['EMA4']), 'MAtrend'] = 10                      
        data.loc[(data['EMA2']>data['EMA3'])&(data['EMA3']<data['EMA4']), 'MAtrend'] = 5             
        data.loc[(data['EMA2']<data['EMA3'])&(data['EMA3']>data['EMA4']), 'MAtrend'] = 5
        
        # Calculating HvsC and LvsC variables for Longs and Shorts Entries respectively
        data['HvsC_ratio'] = np.zeros(len(data))                       # Highs vs Close distance for Long Entries
        data.loc[((data['High']-data['Open'])>0)&((data['Close']-data['Open'])>0), 'HvsC_ratio'] = (data['Close']-data['Open'])/(data['High']-data['Open']) # Pct of the range of the bar 
        data['LvsC_ratio'] = np.zeros(len(data))
        data.loc[((data['Open']-data['Low'])>0)&((data['Open']-data['Close'])>0), 'LvsC_ratio'] = (data['Open']-data['Close'])/(data['Open']-data['Low'])
        
        # For Calculating the Trend Filter, the 3 last variables will be used to build a signal of: 1, 0 or -1 to allow or not Entry Rule execution   
        data['TrendFilter'] = np.zeros(len(data))
        data.loc[((data['MAtrend']>5)&(data['HvsC_ratio']>0.65)&(data['MAratiosDif']>StrengthLE)), 'TrendFilter'] = 1
        data.loc[((data['MAtrend']<10)&(data['LvsC_ratio']>0.65)&(data['MAratiosDif']<StrengthSE)), 'TrendFilter'] = -1 
        
        # Calculating Donchian Channels
        data['Highline'] = data['High'].rolling(DnchnUp).max().shift(1)       #Upper Band - Donchian Breakout
        data['Lowline'] = data['Low'].rolling(DnchnDn).min().shift(1)         #Lower Band - Donchian Breakout 
        
        # Creating columns for signals, positions and MtoM PnL etc
        data['Buy_Signal'] = np.zeros(len(data))                # Buy_Signal = 1 means Bought
        data['Sell_Signal'] = np.zeros(len(data))               # Sell_Signal = -1 mean Sold
        data['MktPos'] = np.zeros(len(data))                    # Will be 1 for Longs, 0 Flat, -1 for Shorts
        data['EntryPrice'] = np.zeros(len(data))                # Will save Entry Price when occurs for Longs or Shorts 
        data['ExitPrice'] = np.zeros(len(data))                 # Will save Exit Price when occurs for Longs or Shorts
        data['ExitRule'] = ''                                   # Will save a string describing the Exit Rule used for each trade at the end
        data['MtoM'] = np.zeros(len(data))                      # Mark to Market will compute current cumultive trade P&L in price point values
        data['MtoM_Chg'] = np.zeros(len(data))                  # Compute the Bar to Bar change of MtoM
        data['Daily_P&L'] = np.zeros(len(data))                 # USD P&L multipliying MtoM Chg by the point value (on 1 contract)
        data['Longs'] = np.zeros(len(data))                     # Will count Long trades at the end
        data['Shorts'] = np.zeros(len(data))                    # Will count Short trades at the end
        data['Trade_PnL'] = np.zeros(len(data))                 # Compute total USD P&L at the end of the trade
        data['MaxMtM'] = np.zeros(len(data))                    # Max of MtoM per trade will be used to compute when some Pct of profit is achieved
        data['BestClose'] = data['Close']                       # Best Close price represents max P&L achieved for Longs and Shorts trades used for TE Rule
        data['Log_Return'] = np.zeros(len(data))                # Use MtoM to compute Ln Returns for each bar used to compute Sharpe Ratio
        data['Trade_Return'] = np.zeros(len(data))              # Simple Return at the end of each trade calculated when Exit occurs 
        data['Equity'] = np.ones(len(data))*Initial_Capital
        data = data.dropna()
        
        N = len(data)
        data.index = range(0,N)
        for i in range(1,N):
            time = dt.datetime.strptime(data['Time'][i], '%H:%M:%S').time()
            ################################################ IF WE ARE FLAT THEN... #################################################
            if (data['MktPos'][i-1] == 0):
                data.set_value(i-1,'BestClose', 0)
                ##################################### START ENTRY RULE FOR LONGS #######################################
                if (time < dt.time(15,0,0)) and ((data['Close'][i-1] > data['Highline'][i-1]) and (data['TrendFilter'][i-1] > 0)):   
                    entrybar = i
                    data.set_value(i,'Buy_Signal', 1)
                    entryprice = data['Open'][i]
                    data.set_value(i,'EntryPrice', entryprice)
                    Stop_Price = round((entryprice*(100-StpPctLE)/100)*Ticks_per_Point)/Ticks_per_Point  # Rounding to get a real Future Price with Ticks
                    if (data['Low'][i] < Stop_Price):                # This If condition is for Stop Losses on Entry bar "bad entries"
                        data.set_value(i,'Sell_Signal', -1)
                        data.set_value(i,'Longs', 1)
                        data.set_value(i,'MktPos', 0)
                        exitprice = Stop_Price
                        data.set_value(i,'ExitPrice', exitprice)
                        data.set_value(i,'ExitRule', 'Stop-Loss')
                        data.set_value(i,'BestClose', 0)
                        data.set_value(i,'MtoM', exitprice - entryprice)
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                        InvestedEq = data['Equity'][i-1]
                        #ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                        ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                        data.set_value(i,'Trade_Return',ret)
                        data.set_value(i,'Equity', InvestedEq + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                    else:
                        data.set_value(i,'MktPos', 1)
                        data.set_value(i,'BestClose', data['Close'][entrybar:(i+1)].max())
                        data.set_value(i,'MtoM', data['Close'][i] - entryprice)
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i])
                        data.set_value(i,'MaxMtM', data['MtoM'][entrybar:(i+1)].max())
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        InvestedEq = data['Equity'][i-1]
                        data.set_value(i,'Equity', InvestedEq + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                ##################################### START ENTRY RULE FOR SHORTS #######################################
                elif (time < dt.time(15,0,0)) and ((data['Close'][i-1] < data['Lowline'][i-1]) and (data['TrendFilter'][i-1] < 0)): 
                    entrybar = i
                    data.set_value(i,'Sell_Signal', -1)
                    entryprice = data['Open'][i]
                    data.set_value(i,'EntryPrice', entryprice)
                    Stop_Price = round((entryprice*(100+StpPctSE)/100)*Ticks_per_Point)/Ticks_per_Point
                    if (data['High'][i] > Stop_Price):
                        data.set_value(i,'Buy_Signal', 1)
                        data.set_value(i,'Shorts', 1)
                        data.set_value(i,'MktPos', 0)
                        exitprice = Stop_Price
                        data.set_value(i,'ExitPrice', exitprice)
                        data.set_value(i,'ExitRule', 'Stop-Loss')
                        data.set_value(i,'BestClose', 0)
                        data.set_value(i,'MtoM', entryprice - exitprice)
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                        InvestedEq = data['Equity'][i-1]
                        #ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                        ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                        data.set_value(i,'Trade_Return',ret)
                        data.set_value(i,'Equity', InvestedEq + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                    else:
                        data.set_value(i,'MktPos', -1)
                        data.set_value(i,'BestClose', data['Close'][entrybar:(i+1)].min())
                        data.set_value(i,'MtoM', entryprice - data['Close'][i])
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i])
                        data.set_value(i,'MaxMtM', data['MtoM'][entrybar:(i+1)].max())
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        InvestedEq = data['Equity'][i-1]
                        data.set_value(i,'Equity', InvestedEq + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                else:
                    data.set_value(i,'Equity', data['Equity'][i-1])           # If there is no trade then Equity remains at the same value
            ################################################### IF WE ARE LONG THEN... ###########################################
            elif (data['MktPos'][i-1] == 1):
                if time == dt.time(15,0,0):
                    data.set_value(i,'Sell_Signal', -1)
                    data.set_value(i,'Longs', 1)                               
                    data.set_value(i,'MktPos', 0)
                    exitprice = round(((0.75*(data['BestClose'][i-1]-entryprice))+entryprice)*Ticks_per_Point)/Ticks_per_Point
                    data.set_value(i,'ExitPrice', exitprice)
                    data.set_value(i,'MtoM', 0)
                    data.set_value(i,'MtoM_Chg', exitprice - data['Close'][i-1])
                    data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                    data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                    data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                    data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                    #ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                    ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                    data.set_value(i,'Trade_Return',ret)
                    data.set_value(i,'ExitRule', 'Time_Xt')
                elif ((((data['BestClose'][i-1]/entryprice)-1)*100) > ProfTriggLE):
                    ################################################### TRAILING EXIT  ###########################################
                    if (data['Low'][i] < round(((0.75*(data['BestClose'][i-1]-entryprice))+entryprice)*Ticks_per_Point)/Ticks_per_Point):
                        data.set_value(i,'Sell_Signal', -1)
                        data.set_value(i,'Longs', 1)                               
                        data.set_value(i,'MktPos', 0)
                        exitprice = round(((0.75*(data['BestClose'][i-1]-entryprice))+entryprice)*Ticks_per_Point)/Ticks_per_Point
                        data.set_value(i,'ExitPrice', exitprice)
                        data.set_value(i,'MtoM', 0)
                        data.set_value(i,'MtoM_Chg', exitprice - data['Close'][i-1])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                        #ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                        ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                        data.set_value(i,'Trade_Return',ret)
                        data.set_value(i,'ExitRule', 'Trailing_Xt')
                    else:
                        data.set_value(i,'MktPos', 1)                           
                        data.set_value(i,'MtoM', data['Close'][i] - entryprice)
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i]-data['MtoM'][i-1])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'MaxMtM', data['MtoM'][entrybar:(i+1)].max())
                        data.set_value(i,'BestClose', data['Close'][entrybar:(i+1)].max())
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                elif ((((data['BestClose'][i-1]/entryprice)-1)*100) < ProfTriggLE):
                    ################################################### STOP LOSS  ###########################################
                    if (data['Low'][i] < Stop_Price):     
                        data.set_value(i,'Sell_Signal', -1)       # If Trailing exit condition never occurs then we wait until the Protective Stop level
                        data.set_value(i,'Longs', 1)
                        data.set_value(i,'MktPos', 0)
                        exitprice = Stop_Price
                        data.set_value(i,'ExitPrice', exitprice)
                        data.set_value(i,'MtoM', 0)
                        data.set_value(i,'MtoM_Chg', exitprice - data['Close'][i-1])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                        #ret = (((exitprice - entryprice)*Point_Value)/InvestedEq)*100
                        ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                        data.set_value(i,'Trade_Return',ret)
                        data.set_value(i,'ExitRule', 'Stop-Loss')
                    else:
                        data.set_value(i,'MktPos', 1)                           
                        data.set_value(i,'MtoM', data['Close'][i] - entryprice)
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i]-data['MtoM'][i-1])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'MaxMtM', data['MtoM'][entrybar:(i+1)].max())
                        data.set_value(i,'BestClose', data['Close'][entrybar:(i+1)].max())
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
            ################################################### IF WE ARE SHORT THEN... ###########################################
            elif (data['MktPos'][i-1] == -1):
                if time == dt.time(15,0,0):
                    data.set_value(i,'Buy_Signal', 1)
                    data.set_value(i,'Shorts', 1)
                    data.set_value(i,'MktPos', 0)
                    exitprice = round((entryprice - (0.75*(entryprice - data['BestClose'][i-1])))*Ticks_per_Point)/Ticks_per_Point
                    data.set_value(i,'ExitPrice', exitprice)
                    data.set_value(i,'MtoM', 0)
                    data.set_value(i,'MtoM_Chg', data['Close'][i-1] - exitprice)
                    data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                    data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                    data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                    data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                    #ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                    ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                    data.set_value(i,'Trade_Return',ret)
                    data.set_value(i,'ExitRule', 'Time_Xt')
                elif ((((entryprice/data['BestClose'][i-1])-1)*100) > ProfTriggSE):
                    ################################################### TRAILING EXIT  ###########################################
                    if (data['High'][i] > round((entryprice - (0.75*(entryprice - data['BestClose'][i-1])))*Ticks_per_Point)/Ticks_per_Point):
                        data.set_value(i,'Buy_Signal', 1)
                        data.set_value(i,'Shorts', 1)
                        data.set_value(i,'MktPos', 0)
                        exitprice = round((entryprice - (0.75*(entryprice - data['BestClose'][i-1])))*Ticks_per_Point)/Ticks_per_Point
                        data.set_value(i,'ExitPrice', exitprice)
                        data.set_value(i,'MtoM', 0)
                        data.set_value(i,'MtoM_Chg', data['Close'][i-1] - exitprice)
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                        #ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                        ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                        data.set_value(i,'Trade_Return',ret)
                        data.set_value(i,'ExitRule', 'Trailing_Xt')
                    else:
                        data.set_value(i,'MktPos', -1)
                        data.set_value(i,'MtoM', entryprice-data['Close'][i])
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i]-data['MtoM'][i-1])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'MaxMtM', data['MtoM'][entrybar:(i+1)].max())
                        data.set_value(i,'BestClose', data['Close'][entrybar:(i+1)].min())
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                elif ((((entryprice/data['BestClose'][i-1])-1)*100) < ProfTriggSE):
                    ################################################### STOP LOSS ###########################################
                    if (data['High'][i] > Stop_Price):
                        data.set_value(i,'Buy_Signal', 1)
                        data.set_value(i,'Shorts', 1)
                        data.set_value(i,'MktPos', 0)
                        exitprice = Stop_Price
                        data.set_value(i,'ExitPrice', exitprice)
                        data.set_value(i,'MtoM', 0)
                        data.set_value(i,'MtoM_Chg', data['Close'][i-1] - exitprice)
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
                        data.set_value(i,'Trade_PnL', (data['MtoM_Chg'][entrybar:(i+1)].sum()*Point_Value)-self.__tradeCost)
                        #ret = (((entryprice-exitprice)*Point_Value)/InvestedEq)*100
                        ret = (data['Trade_PnL'].iloc[i]/InvestedEq)*100.0
                        data.set_value(i,'Trade_Return',ret)
                        data.set_value(i,'ExitRule', 'Stop-Loss')
                    else:
                        data.set_value(i,'MktPos', -1)
                        data.set_value(i,'MtoM', entryprice-data['Close'][i])
                        data.set_value(i,'MtoM_Chg', data['MtoM'][i]-data['MtoM'][i-1])
                        data.set_value(i,'Daily_P&L', data['MtoM_Chg'][i]*Point_Value)
                        data.set_value(i,'MaxMtM', data['MtoM'][entrybar:(i+1)].max())
                        data.set_value(i,'BestClose', data['Close'][entrybar:(i+1)].min())
                        data.set_value(i,'Equity', data['Equity'][i-1] + data['Daily_P&L'][i])
                        data.set_value(i,'Log_Return', np.log(data['Equity'][i]/data['Equity'][i-1]))
        
        if getDailyReturns:
            
            columnsReqd = ['Date', 
                           'Time', 
                           'Symbol', 
                           'Close',
                           'EntryPrice', 
                           'ExitPrice', 
                           'Longs', 
                           'Shorts',
                           'Equity'
                          ]
            analyzerData = data[columnsReqd]
            return (self.getReturns(analyzerData), 
                    self.getBuyAndHoldReturns(analyzerData), 
                    self.getTransactions(analyzerData, Point_Value)
                    )             
        
        df = self.calcStats(timeframe, data, self.__initCap, combo)
        return df
        
    def combosGenerator(self):
        timeframe = [15]
        #ema1 = [3,5]
        #ema2 = [5,8]
        #ema3 = [9,12]
        #ema4 = [20,25]
        
        ema1 = [7]
        ema2 = [12]
        ema3 = [25]
        ema4 = [50]
        
        dnchnUp = [10,20,30]
        dnchnDn = [10,20,30]
        
        profTriggLE = [0.2,0.3,0.4]
        stpPctLE = [0.1,0.2,0.3]
        strengthLE = [0.15,0.20,0.25]
        
        profTriggSE = [0.2,0.3,0.4]
        stpPctSE = [0.1,0.2,0.3]
        strengthSE = [-0.15,-0.20,-0.25]
        
        return list(itertools.product(timeframe, ema1, ema2, ema3, ema4,
                                      dnchnUp, dnchnDn, profTriggLE, stpPctLE, 
                                      strengthLE, profTriggSE, stpPctSE, 
                                      strengthSE))