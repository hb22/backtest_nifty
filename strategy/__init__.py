#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 19:04:15 2018

@author: himanshu
"""

import numpy as np
import pandas as pd
import datetime as dt
import pytz

from abc import ABC, abstractmethod

class Strategy(ABC):
    
    def __init__(self):
        super().__init__()
        self.__transactions = None
        
        
    @abstractmethod
    def run(self):
        pass
    
    @abstractmethod
    def combosGenerator(self):
        pass
    
    def calcStats(self, timeframe, data, initCap, combo):
        
        N = len(data)
        data['MaxEquity'] = np.ones(N)*initCap
        for j in range(0,N):
            data.set_value(j,'MaxEquity',data['Equity'][0:(j+1)].max())
        data['DD'] = round((data['Equity']/data['MaxEquity']-1)*100,2)
        MaxDD = data['DD'].min()
        
        Total_Return = round(((data['Equity'].iloc[-1]/initCap)-1)*100,2)
        Total_Trades = len(data['Trade_Return'].loc[data['Trade_Return'] != 0])
        
        Winners = len(data['Trade_Return'].loc[data['Trade_Return'] > 0])
        if (Total_Trades == 0):
            Hit_Ratio = 0
        else:
            Hit_Ratio = round((Winners/Total_Trades)*100,2)
        
        Avg_Profit = np.mean(data['Trade_Return'].loc[data['Trade_Return'] > 0])
        Avg_Loss = np.mean(data['Trade_Return'].loc[data['Trade_Return'] < 0])
        
        Avg_PL_Ratio = round(Avg_Profit/np.abs(Avg_Loss),2)
        
        if timeframe is None:
            numOfCandles = 252
        else:
            numOfCandles = 252*6.25*(60/timeframe)
        
        AvgLnRtn = np.mean(data['Log_Return'])
        Std = np.std(data['Log_Return'])
        if (Std == 0):
            SharpeRatio = 0
        else:
            SharpeRatio = round((AvgLnRtn/Std)*np.sqrt(numOfCandles),2)      #Annualized Sharpe Ratio its calculated using Daily LN Returns
        
        Std = np.std(data['Log_Return'].loc[data['Log_Return'] < 0])
        if (Std == 0):
            SortinoRatio = 0
        else:
            SortinoRatio = round((AvgLnRtn/Std)*np.sqrt(numOfCandles),2)      #Annualized Sharpe Ratio its calculated using Daily LN Returns
        
        
        Years = N/numOfCandles
        Cagr = round((((1+(Total_Return/100))**(float(1)/Years))-1)*100,2) 
        
        df = pd.DataFrame(index = [combo])
        
        df.loc[combo, 'Initial Capital'] = initCap
        df.loc[combo, 'Ending Capital'] = data['Equity'].iloc[-1]
        df.loc[combo, 'Total Return'] = Total_Return
        df.loc[combo, 'CAGR'] = Cagr
        df.loc[combo, 'Max Drawdown'] = MaxDD
        df.loc[combo, 'Sharpe Ratio'] = SharpeRatio
        df.loc[combo, 'Sortino Ratio'] = SortinoRatio

        df.loc[combo, 'Total Trades'] = Total_Trades
        df.loc[combo, 'Hit Ratio'] = Hit_Ratio
        df.loc[combo, 'Avg. Profit'] = Avg_Profit
        df.loc[combo, 'Avg. Loss'] = Avg_Loss
        df.loc[combo, 'Profit Factor'] = Avg_PL_Ratio
        
        return df
    
    def getReturns(self, data):
        
        try:
            dateTime = data['Date'] + ' ' + data['Time']
            dateTime = pd.to_datetime(dateTime, format='%m/%d/%Y %H:%M:%S')
        except:
            dateTime = data['Date']
            dateTime = pd.to_datetime(dateTime, format='%m/%d/%Y')
        
        #dailyRets = (data['Equity']/data['Equity'].shift(1)) - 1
        dailyRets = np.log(data['Equity']/data['Equity'].shift(1))
        dailyRets.index = dateTime
        dailyRets.index = dailyRets.index.tz_localize(pytz.timezone('US/Central'))
        
        dailyRets.name = data['Symbol'].iloc[0]
        self.__returns = dailyRets
        return self.__returns.dropna()
    
    def getBuyAndHoldReturns(self, data):
        
        try:
            dateTime = data['Date'] + ' ' + data['Time']
            dateTime = pd.to_datetime(dateTime, format='%m/%d/%Y %H:%M:%S')
        except:
            dateTime = data['Date']
            dateTime = pd.to_datetime(dateTime, format='%m/%d/%Y')
        
        buyAndHoldRets = np.log(data['Close']/data['Close'].shift(1))
        buyAndHoldRets.index = dateTime
        buyAndHoldRets.index = buyAndHoldRets.index.tz_localize(pytz.timezone('US/Central'))
        
        buyAndHoldRets.name = 'BuyAndHold'
        return buyAndHoldRets.dropna()
    
    def getTransactions(self, data, pointValue=1):
        cols = ['Trade', 'Entry Time', 'Entry Price', 
                'Exit Time', 'Exit Price', 'PnL', 'Equity']
    
        transactions = pd.DataFrame(columns=cols)
        idx = 0
        
        enteries = data['EntryPrice'].loc[data['EntryPrice'] != 0]
        exits = data['ExitPrice'].loc[data['ExitPrice'] != 0]
        
        enterIndices = enteries.index.tolist()
        exitIndices = exits.index.tolist()
        
        for i in range(len(enterIndices)):
            
            entryPos = enterIndices[i]
            entryDateTime = data['Date'].loc[entryPos] + ' ' + data['Time'].loc[entryPos]
            entryDateTime = dt.datetime.strptime(entryDateTime, '%m/%d/%Y %H:%M:%S')
            entryPrice = data['EntryPrice'].loc[entryPos]
            entryEquity = data['Equity'].loc[entryPos]
            
            exitPos = exitIndices[i]
            exitDateTime = data['Date'].loc[exitPos] + ' ' + data['Time'].loc[exitPos]
            exitDateTime = dt.datetime.strptime(exitDateTime, '%m/%d/%Y %H:%M:%S')
            exitPrice = data['ExitPrice'].loc[exitPos]
            longs = data['Longs'].loc[exitPos]
            shorts = data['Shorts'].loc[exitPos]
            
            pnl = 0.0
            if longs == 1:
                trade = 'BUY'
                pnl = (exitPrice - entryPrice)*pointValue
            elif shorts == 1:
                trade = 'SELL'
                pnl = (-1)*(exitPrice - entryPrice)*pointValue
            
            #ret = (pnl/entryEquity)*100.0
            row = [trade,entryDateTime,entryPrice,
                   exitDateTime,exitPrice,pnl,entryEquity]
        
            transactions.loc[idx] = row
            idx+=1         
        
        return transactions
